#ifndef RECT_H
#define RECT_H
#include<iostream>
using namespace std;
class Rect
{
private:
    int length;
    int width;

public:
    void getSides();
    void makeShape();

};

#endif // RECT_H
